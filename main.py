#!/usr/bin/python
# -*- coding: utf-8 -*-
import os 
import webapp2
import f_prec 
from google.appengine.ext import ndb
from google.appengine.ext.webapp import template
from time import gmtime, strftime


list_key = []
class Data(ndb.Model):
    item_key = ndb.StringProperty()
    item = ndb.StringProperty()
    time = ndb.StringProperty()
    ids = ndb.StringProperty()


def createD(a , b, c):
    global list_key
    s_data = Data(item_key = a, item = b, time  = (strftime("%H:%M +0003", gmtime())), ids = c)
    s_key = s_data.put()
    list_key.append(s_key) 
    return (s_key,s_data)


class MainPage(webapp2.RequestHandler):
 
    def get(self):
        path = os.path.join(os.path.dirname(__file__), 'data.html')
        self.response.out.write(template.render(path, {}))
         
             
    def post(self):
        extra_k = self.request.get('key')
        extra_m = self.request.get('mes')
        extra_id = self.request.get('id')
        if extra_k == '3':
            ans = f_prec.command(extra_m)
            if len(ans)>100:
                for i in range (len(ans) // 100+1):
                    if (len(ans)>=100):
                        add = ans[:100]
                        ans = ans[100:]
                    else:
                        add = ans
                    createD('2',add,">")
            else:
                createD('2',ans,">")
        else:
            createD("1",extra_m,">")

class Computer(webapp2.RequestHandler):


    def get(self):
        global list_key
        ind = -1
        for i in range (len(list_key)):
            a = list_key[i].get()
            try:
                if a.item_key == "1":
                    item = a.item
                    key = list_key[i]
                    time = a.time
                    ids = a.ids
                    ind = ind + 1
                    self.response.out.write("<div class = '"+str(ind)+"'><h3>Question</h3>")
                    self.response.out.write("<p><span class = 'item'>" + (str(item))+"</span><p>")
                    self.response.out.write("<p><span class = 'time'>" + (str(time))+"</span><p>")
                    self.response.out.write("<p><span class = 'id'>"+ (str(ids))+"</span><p>")
                    self.response.out.write("</div>")
            except AttributeError:
                self.response.out.write("<h3>ERROR</h3>")
    def post(self):
        global list_key
        dell = self.request.get('dell')
        lk = []
        if dell == '1' :
            for i in range (len(list_key)):
                a = list_key[i].get()
                if a.item_key == "1" :
                    lk.append(list_key[i])
                    key = list_key[i]
                    key.delete()
            #list_key = [str(elem) for elem in list_key]
            for o in range (len(lk)):
                u = 0
                for u in range(len(list_key)-1):
                    if lk[o] == list_key[u]:
                        list_key = list_key[:u]+list_key[u+1:]
            if list_key[len(list_key)-1] in lk:
                list_key = list_key [:len(list_key)-1] 
            
                
            
class Phone(webapp2.RequestHandler):
    def get(self):
        global list_key
        for i in range (len(list_key)):
            a = list_key[i].get()
            try:
                if a.item_key == "2" :
                    ids = a.ids
                    item = a.item
                    self.response.write((str(ids))+'>'+(str(item)))
                    
            except AttributeError:
                self.response.out.write("<h3>ERROR</h3>")
    def post(self):
        global list_key
        dell = self.request.get('dell')
        lk = []
        if dell == '2' :
            for i in range (len(list_key)):
                a = list_key[i].get()
                if a.item_key == "2" :
                    lk.append(list_key[i])
                    key = list_key[i]
                    key.delete()
            #list_key = [str(elem) for elem in list_key]
            for o in range (len(lk)):
                u = 0
                for u in range(len(list_key)-1):
                    if lk[o] == list_key[u]:
                        list_key = list_key[:u]+list_key[u+1:]
            if list_key[len(list_key)-1] in lk:
                list_key = list_key [:len(list_key)-1] 


        
        


application = webapp2.WSGIApplication ([('/', MainPage),('/1', Computer),('/2',Phone)],debug=True)







